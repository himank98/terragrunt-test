locals {
  environment_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  env = local.environment_vars.locals.environment
}

terraform {
  source = "/root/terragrunt-test/modules/compute"
}

include {
  path = find_in_parent_folders()
}

inputs = {
    ec2_instance_type = "t2.micro" 
    subnet_id1 = "subnet-751ca67b"
    key_name = "docker"
    ec2_ami = "ami-0915bcb5fa77e4892"
    availability_zone1 = "us-east-1f"
}
