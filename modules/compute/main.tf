// AWS INSTANCE WITHOUT AUTOSCALING
resource "aws_instance" "EC2Webserver1" {
  ami                    = var.ec2_ami
  instance_type          = var.ec2_instance_type
  subnet_id              = var.subnet_id1 //us-east-1c
  key_name               = var.key_name
  availability_zone      = var.availability_zone1 //us-east-1c
  tenancy                = "default"
  tags = {
    Name = "EC2Webserver1"
  }
}
